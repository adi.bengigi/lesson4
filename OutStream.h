#pragma once
#include <stdio.h>

class OutStream
{

protected:
	FILE * _theFile;
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE* theFile));
};


void endline(FILE* theFile);


