#include "Logger.h"

void Logger::setStartLine()
{
	static unsigned int count = 0;//the static integer that counts our new lines
	if (_startLine)
	{
		count++;//new line was added
		os << count << " LOG ";//prints needed details
		_startLine = false;//not a new line anymore
	}
}
Logger::Logger()
{
	_startLine = true;//starts a new line
}
Logger::~Logger()
{
}
Logger& operator<<(Logger& l, const char *msg)
{
	l.setStartLine();
	l.os << msg;//prints the massage
	return l;
}
Logger& operator<<(Logger& l, int num)
{
	l.setStartLine();
	l.os << num;//prints the number
	return l;
}
Logger& operator<<(Logger& l, void(*pf)(FILE*))
{
	l._startLine = true;//its a new line!
	l.os << pf;
	return l;
}