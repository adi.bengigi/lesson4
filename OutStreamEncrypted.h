#pragma once
#include "OutStream.h"
#define FIRST_ASCII 32
#define LAST_ASCII 126
#define MAX 100
class OutStreamEncrypted :public OutStream
{
private:
	int _move;
public:
	OutStreamEncrypted(int move);
	~OutStreamEncrypted();

	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE* theFile));
};

