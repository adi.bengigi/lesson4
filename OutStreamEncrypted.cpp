#include "OutStreamEncrypted.h"
#include <string>

OutStreamEncrypted::OutStreamEncrypted(int move)
{
	this->_move = move;//init the integer
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	char newstr[MAX] = { 0 };
	char first = 0;
	char last = 0;
	int i = 0;

	first = FIRST_ASCII;//first ascii value
	last = LAST_ASCII;//last ascii value

	for (i = 0; str[i]; i++)//for every letter in string
	{
		newstr[i] = (str[i] - first + this->_move) % (last - first + 1) + first;//calculates the letter after description
	}
	OutStream::operator<<(newstr);//prints with the original function
	return *this;
}
OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	std::string number;
	char str[MAX] = { 0 };
	int i = 0;

	number = std::to_string(num);//makes the number a string
	for (i = 0; number[i]; i++)
	{
		str[i] = number[i];//moves every char in string to the char *
	}
	this->operator<<(str);//sends to the string function to print
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE* theFile))
{
	OutStream::operator<<(pf);//calls the needed function
	return *this;
}