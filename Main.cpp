#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"


int main(int argc, char **argv)
{
	//ex1
	OutStream os;
	os << "I am the doctor and I'm " << 1500 << " years old" << endline;

	//ex2
	FileStream fs;
	fs << "I am the doctor and I'm " << 1500 << " years old" << endline;

	//ex3
	OutStreamEncrypted ose(5);
	ose << "I am the doctor and I'm " << 1500 << " years old" << endline;

	//ex4
	Logger logger;
	logger << "I am the doctor and I'm " << 1500 << " years old" << endline;
	//ex5
	logger << "new line" << endline;
	logger << "boom X" << 3 << endline;

	return 0;
}