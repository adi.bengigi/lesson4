#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
	this->_theFile = stdout;//print to file is now print to screen
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->_theFile, "%s", str);//prints string to the file
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_theFile, "%d", num);//prints num to the file
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE* theFile))
{
	pf(this->_theFile);//prints a new line (by the function)
	return *this;
}


void endline(FILE* theFile)
{
	fprintf(theFile, "\n");
}
